--liquibase formatted sql

--changeset vadymusyk@gmail.com:0

CREATE SCHEMA IF NOT EXISTS test1 AUTHORIZATION postgres;

CREATE TABLE account (
  id       INTEGER PRIMARY KEY,
  username VARCHAR(50) NOT NULL UNIQUE,
  password VARCHAR(20) NOT NULL,
  role     VARCHAR(30) NOT NULL,
  active BOOLEAN DEFAULT TRUE
);

INSERT INTO account (id, username, password, role) VALUES (1, 'admin', 'MJ+d3Fg7_', 'ADMIN');
INSERT INTO account (id, username, password, role) VALUES (2, 'gray', 'graygray', 'MODERATOR');
INSERT INTO account (id, username, password, role) VALUES (3, 'user1', '123', 'USER');