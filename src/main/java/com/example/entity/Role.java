package com.example.entity;

/**
 * Created by vadym on 26.12.2016.
 */
public enum Role {
    ADMIN,
    MODERATOR,
    USER
}
